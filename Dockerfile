FROM python:latest
WORKDIR /src
COPY ./src .
CMD ["python", "/src/main.py"]
